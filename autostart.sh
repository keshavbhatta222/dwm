#!/bin/sh
sxhkd &
~/.fehbg &
dwmblocks 2>&1 >/dev/null &
picom -f --experimental-backend -b &
xrdb -load ~/.Xresources
setxkbmap -option caps:escape
dunst
