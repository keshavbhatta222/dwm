static char normbgcolor[] = "#1e1e2e";
static char normbordercolor[] = "#1e1e2e";
static char normfgcolor[] = "#cdd6f4";
static char selfgcolor[] = "#282828";
static char selbordercolor[] = "#1e1e2e";
static char selbgcolor[] = "#89b4fa";
static char *colors[][3] = {
    /*               fg           bg           border   */
    [SchemeNorm] = {normfgcolor, normbgcolor, normbordercolor},
    [SchemeSel] = {selfgcolor, selbgcolor, selbordercolor},
};
