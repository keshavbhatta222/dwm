static char normbgcolor[] = "#282c34";
static char normbordercolor[] = "#282c34";
static char normfgcolor[] = "#bbc2cf";
static char selfgcolor[] = "#282c34";
static char selbordercolor[] = "#282c34";
static char selbgcolor[] = "#51afef";
static char *colors[][3] = {
    /*               fg           bg           border   */
    [SchemeNorm] = {normfgcolor, normbgcolor, normbordercolor},
    [SchemeSel] = {selfgcolor, selbgcolor, selbordercolor},
};
