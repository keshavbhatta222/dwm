static char normbgcolor[] = "#282828";
static char normbordercolor[] = "#1e1e2e";
static char normfgcolor[] = "#ebdbb2";
static char selfgcolor[] = "#282828";
static char selbordercolor[] = "#1e1e2e";
static char selbgcolor[] = "#458588";
static char *colors[][3] = {
    /*               fg           bg           border   */
    [SchemeNorm] = {normfgcolor, normbgcolor, normbordercolor},
    [SchemeSel] = {selfgcolor, selbgcolor, selbordercolor},
};
